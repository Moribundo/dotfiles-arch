" do not load defaults if ~/.vimrc is missing
"let skip_defaults_vim=1

" Mis configs

" Tema
colorscheme torte

" Texto destacado
syntax on

" Mostrar número de linea
set number

" Hacer backup al editar
set backup

" Dejar el backup en el directorio actual
set backupdir=.

" Mostrar 256 colores
set t_Co=25

" Autocompletar
inoremap ( ()<Esc>i
inoremap " ""<Esc>i
inoremap [ []<Esc>i
inoremap { {}<Esc>i
inoremap < <><Esc>i

" Especificar formato de archivo
autocmd BufRead,BufNewFile *.md set filetype=markdown
autocmd BufRead,BufNewFile *.hbs set filetype=html
autocmd BufRead,BufNewFile *.ejs set filetype=html
autocmd BufRead,BufNewFile *.njk set filetype=html

"Resaltado de búsqueda
set hlsearch

" Remap con CTRL+L para limpiar resaltado
nnoremap <C-L> :nohls<cr><C-L>

" set spell spelllang=es_es