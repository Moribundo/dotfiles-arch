#!/bin/bash
# Example Bar Action Script for Linux.
# Requires: acpi, iostat, lm-sensors, aptitude.
# Tested on: Debian Buster(with newest spectrwm built from source), Debian Bullseye, Devuan Chimaera, Devuan Ceres
# This config can be found on github.com/linuxdabbler

hostname="${HOSTNAME:-${hostname:-$(hostname)}}"

##############################
#	    DISK
##############################

hddicon() {
    echo "HD: "
}

hdd() {
	  free="$(df -h /home | grep /dev | awk '{print $3}' | sed 's/G/Gb/')"
      perc="$(df -h /home | grep /dev/ | awk '{print $5}')"
      echo "$perc  ($free)"
    }

##############################
#	    CPU
##############################
cpuicon() {
    echo "CPU:"
}

cpu() {
	  read cpu a b c previdle rest < /proc/stat
	    prevtotal=$((a+b+c+previdle))
	      sleep 0.5
	        read cpu a b c idle rest < /proc/stat
		  total=$((a+b+c+idle))
		    cpu=$((100*( (total-prevtotal) - (idle-previdle) ) / (total-prevtotal) ))
            echo "$cpu%"
	      }
##############################
#	    VOLUME
##############################
volicon() {
    echo "Vol:"
}

vol() {

    VOL="$(amixer -D pulse get Master | grep Left: | sed 's/[][]//g' | awk '{print $5}')"
    echo "$VOL"

##############################
#	    TIME
##############################
}
clockinfo() {
    echo $(date "+%R")
}

SLEEP_SEC=2
      #loops forever outputting a line every SLEEP_SEC secs
      while :; do
        # echo "$(cpu) | $(mem) | $(pkgs) | $(upgrades) | $(hdd) | $(vpn) $(network) | $(vol) | $(WEATHER) $(TEMP) "
        echo "+@fg=1; $(cpuicon) +@fg=0; $(cpu)\
    +@fg=3; $(hddicon) +@fg=0; $(hdd)\
    +@fg=5; $(volicon) +@fg=0; $(vol)\
    +@fg=1; $(dateinfo) +@fg=4; $(clockicon) +@fg=0; $(clockinfo)\
    "
        sleep $SLEEP_SEC
		done