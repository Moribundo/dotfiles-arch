# .bash_profile

# Get the aliases and functions
[ -f $HOME/.bashrc ] && . $HOME/.bashrc
PS1='\e[0;32m[\u@\h \w]\$ \e[m'
exec startx
